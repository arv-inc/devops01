# NodeJS service
FROM node

# Create folder for container files
RUN mkdir /skill_devops00

# Go to work folder
WORKDIR	/skill_devops00

# cp insall package for one time installation (layer not change every build)
COPY package.json /skill_devops00
RUN yarn install

# Copy files from current folder to container folder
COPY . /skill_devops00

# Test installed app
RUN yarn test

# Build app
RUN yarn build

# Start app after container run
CMD yarn start

# Open 3000 port
EXPOSE 3000
